package com.slashmobility.security.domain.service.impl;

import com.sendgrid.*;
import com.slashmobility.security.domain.entity.UserLogin;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class EmailServiceImplTest {
    private final String activationUrl = "url";
    private final String emailFrom = "emailFrom@email.com";
    private final String subject = "subject";
    private EmailServiceImpl emailService;
    private final static String EMAIL = "email@email.com";
    private final static String RANDOM_TOKEN = "random_tokendas.dasdas";

    @Mock
    private SendGrid sendGrid;

    @BeforeAll
    void setUp() {
        emailService = new EmailServiceImpl(
                activationUrl,
                emailFrom,
                subject,
                sendGrid);
    }

    @AfterEach
    void reset() {
        Mockito.reset(sendGrid);

    }

    @Test
    public void sendActivationEmail() throws IOException {
        when(sendGrid.api(any())).thenReturn(generateResponse());
        UserLogin user = generateEmailUser();
        emailService.sendActivationEmail(user, RANDOM_TOKEN);
        verify(sendGrid, times(1)).api(any());
    }

    @Test
    void validate_email_build() {
        Mail mailExpected = generateMailOk();
        Mail result = emailService.buildMail(generateEmailUser(), buildContent(RANDOM_TOKEN));
        assertEquals(mailExpected.getFrom().getEmail(), result.getFrom().getEmail());
        assertEquals(mailExpected.getSubject(), result.getSubject());
        assertEquals(mailExpected.getPersonalization().size(), result.getPersonalization().size());
        assertEquals(mailExpected.getPersonalization().get(0).getTos().size(), result.getPersonalization().get(0).getTos().size());
        assertEquals(mailExpected.getPersonalization().get(0).getTos().get(0).getEmail(), result.getPersonalization().get(0).getTos().get(0).getEmail());
        assertEquals(mailExpected.getContent().size(), result.getContent().size());
        assertEquals(mailExpected.getContent().get(0).getType(), result.getContent().get(0).getType());
        assertEquals(mailExpected.getContent().get(0).getValue(), result.getContent().get(0).getValue());

    }

    private Response generateResponse() {
        Response response = new Response();
        response.setStatusCode(HttpStatus.ACCEPTED.value());
        return response;
    }

    private UserLogin generateEmailUser() {
        return UserLogin.builder()
                .email(EMAIL)
                .build();
    }

    private Request generateRequestOk() throws IOException {
        Mail mail = generateMailOk();
        Request request = new Request();
        request.setMethod(Method.POST);
        request.setEndpoint("mail/send");
        request.setBody(mail.build());
        return request;

    }

    private Mail generateMailOk() {
        Email from = new Email(emailFrom);
        Email to = new Email(EMAIL);
        Content content = buildContent(RANDOM_TOKEN);
        return new Mail(from, subject, to, content);
    }

    private Content buildContent(String randomToken) {
        return new Content(
                "text/plain",
                "To activate your account go to the following link: "
                        + activationUrl + "?token=" + randomToken);
    }

}
