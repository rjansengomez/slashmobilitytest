package com.slashmobility.security.domain.service.impl;

import com.slashmobility.sdk.security.CacheRepository;
import com.slashmobility.sdk.security.SecurityEncryptor;
import com.slashmobility.sdk.security.SecurityUser;
import com.slashmobility.sdk.security.SecurityWrapper;
import com.slashmobility.security.domain.entity.UserLogin;
import com.slashmobility.security.domain.repository.SecurityRepository;
import com.slashmobility.security.domain.service.EmailService;
import com.slashmobility.security.infrastructure.repository.exception.ActivateTokenNotValidException;
import com.slashmobility.security.infrastructure.repository.exception.EmailAlreadyExistsException;
import com.slashmobility.security.infrastructure.repository.exception.LoginFailedException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static com.slashmobility.security.utils.TestUtils.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class SecurityServiceImplTest {
    @Mock
    private SecurityWrapper wrapper;
    @Mock
    private SecurityRepository repository;
    @Mock
    private EmailService emailService;
    @Mock
    private SecurityEncryptor encryptor;
    @Mock
    private CacheRepository cacheRepository;
    private SecurityServiceImpl service;

    @BeforeAll
    void setUp() {
        this.service = new SecurityServiceImpl(
                wrapper,
                repository,
                emailService,
                encryptor,
                cacheRepository
        );
    }

    @AfterEach
    void reset() {
        Mockito.reset(wrapper);
        Mockito.reset(repository);
        Mockito.reset(emailService);
        Mockito.reset(encryptor);
        Mockito.reset(cacheRepository);
    }

    @Test
    void login() {
        UserLogin userLogin = generateDefault(true, false);
        UserLogin userLogged = generateDefault(true, true);
        when(repository.login(userLogin)).thenReturn(userLogged);
        when(encryptor.createJWT(userLogged.getSecurityUser())).thenReturn("Token");
        String token = service.login(userLogin);
        verify(cacheRepository, times(1)).save("USER_ACTIVE_" + USER, userLogin.getSecurityUser());
        assertEquals("Token", token);
    }


    @Test
    void login_ko() {
        UserLogin userLogin = generateDefault(false, false);
        when(repository.login(userLogin)).thenThrow(new LoginFailedException());
        assertThrows(LoginFailedException.class,
                () -> service.login(userLogin)
        );
    }


    @Test
    void create() {
        UserLogin userLoginCreation = generateDefault(false, false);
        service.create(userLoginCreation);
        verify(repository, times(1)).create(any(), any());
        verify(emailService, times(1)).sendActivationEmail(any(), any());
    }

    @Test
    void create_ko() {
        UserLogin userLoginCreation = generateDefault(false, false);
        doThrow(new EmailAlreadyExistsException()).when(repository).create(any(), any());
        assertThrows(EmailAlreadyExistsException.class,
                () -> service.create(userLoginCreation)
        );
    }

    @Test
    void activate_ok() {
        service.activate(RANDOM_TOKEN);
    }

    @Test
    void activate_ko() {
        when(repository.activate(RANDOM_TOKEN)).thenThrow(new ActivateTokenNotValidException());
        assertThrows(ActivateTokenNotValidException.class,
                () -> service.activate(RANDOM_TOKEN)
        );
    }

    @Test
    void getCurrentUser() {
        when(wrapper.getCurrentUser()).thenReturn(new SecurityUser(USER));
        SecurityUser securityUser = service.getCurrentUser();
        assertEquals(USER, securityUser.getUsername());
    }

    @Test
    void logout() {
        when(wrapper.getCurrentUser()).thenReturn(new SecurityUser(USER));
        service.logout();
        verify(cacheRepository, times(1)).remove("USER_ACTIVE_" + USER, new SecurityUser(USER));
    }


    private UserLogin generateDefault(boolean active, boolean logged) {
        return UserLogin.builder()
                .id(USER)
                .email(EMAIL)
                .active(active)
                .logged(logged)
                .securityUser(new SecurityUser(USER))
                .password(PASSWORD)
                .build();
    }
}
