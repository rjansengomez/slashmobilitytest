package com.slashmobility.security.utils;

import com.slashmobility.sdk.security.SecurityUser;
import com.slashmobility.security.domain.entity.UserLogin;
import com.slashmobility.security.infrastructure.repository.model.UserModel;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class TestUtils {
    public final static String USER = "user";
    public final static String EMAIL = "email@email.com";
    public final static String RANDOM_TOKEN = "random_tokendas.dasdas";
    public final static String PASSWORD = "password";
    public final static String PASSWORD_ENCRYPTED = new BCryptPasswordEncoder().encode(PASSWORD);

    public static UserLogin generateDefaultLogin() {
        return UserLogin.builder()
                .id(USER)
                .securityUser(new SecurityUser(USER))
                .password(PASSWORD)
                .build();
    }

    public static UserModel generateUserOk() {
        UserModel model = new UserModel();
        model.setEmail(EMAIL);
        model.setLogged(false);
        model.setActive(true);
        model.setPassword(PASSWORD_ENCRYPTED);
        model.setUsername(USER);
        model.setActivationToken(RANDOM_TOKEN);
        return model;
    }

    public static UserLogin generateDefaultCreation() {
        return UserLogin.builder()
                .id(USER)
                .email(EMAIL)
                .active(false)
                .logged(false)
                .securityUser(new SecurityUser(USER))
                .password(PASSWORD_ENCRYPTED)
                .build();
    }
}
