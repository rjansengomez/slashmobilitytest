package com.slashmobility.security.infrastructure.entrypoints.rest.v1;

import com.slashmobility.contracts.json.v1.RegisterSecurityUserJsonContract;
import com.slashmobility.security.domain.entity.UserLogin;
import com.slashmobility.security.domain.service.SecurityService;
import com.slashmobility.security.infrastructure.entrypoints.rest.v1.json.PasswordResetJsonRequest;
import com.slashmobility.security.infrastructure.entrypoints.rest.v1.json.UserLoginJsonRequest;
import com.slashmobility.security.infrastructure.entrypoints.rest.v1.mapper.SecurityJsonMapper;
import com.slashmobility.security.infrastructure.repository.exception.ActivateTokenNotValidException;
import com.slashmobility.security.infrastructure.repository.exception.LoginFailedException;
import com.slashmobility.security.infrastructure.repository.exception.UserAlreadyExistsException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static com.slashmobility.security.utils.TestUtils.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;


@ExtendWith(SpringExtension.class)
public class AccessControllerTest {

    @Mock
    private SecurityService securityService;
    @Mock
    private SecurityJsonMapper jsonMapper;
    @Mock
    private HttpServletRequest httpServletRequestMock;

    @Mock
    private HttpSession httpsSessionMock;

    @InjectMocks
    private AccessController controller;

    @Test
    void login() {
        UserLoginJsonRequest loginRequest = generateLoginRequest();
        UserLogin userLogin = generateDefaultLogin();
        when(securityService.login(any())).thenReturn("Token");
        when(securityService.getCurrentUser()).thenReturn(userLogin.getSecurityUser());

        ResponseEntity<String> response = controller.login(loginRequest);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals("Token", response.getBody());

        verify(securityService, times(1)).login(any());
        verify(jsonMapper, times(1)).mapJsonToDomain(loginRequest);
    }

    @Test
    void login_ko() {
        UserLoginJsonRequest loginRequest = generateLoginRequest();
        when(securityService.login(any())).thenThrow(new LoginFailedException());
        assertThrows(LoginFailedException.class,
                () -> controller.login(loginRequest)
        );
        verify(httpsSessionMock, times(0)).setAttribute(eq("USER_ACTIVE_" + USER), any());
        verify(securityService, times(1)).login(any());
        verify(jsonMapper, times(1)).mapJsonToDomain(loginRequest);
    }


    @Test
    void register() {
        RegisterSecurityUserJsonContract creationRequest = generateCreationRequest();
        ResponseEntity<Void> response = controller.create(creationRequest);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        verify(jsonMapper, times(1)).mapJsonToDomain(creationRequest);
        verify(securityService, times(1)).create(any());
    }

    @Test
    void register_ko() {
        RegisterSecurityUserJsonContract creationRequest = generateCreationRequest();
        doThrow(new UserAlreadyExistsException()).when(securityService).create(any());
        assertThrows(UserAlreadyExistsException.class,
                () -> controller.create(creationRequest)
        );
        verify(jsonMapper, times(1)).mapJsonToDomain(creationRequest);
        verify(securityService, times(1)).create(any());
    }

    @Test
    void activate_ok() {
        ResponseEntity<Void> result = controller.activate(RANDOM_TOKEN);
        assertEquals(HttpStatus.OK, result.getStatusCode());
        verify(securityService, times(1)).activate(RANDOM_TOKEN);
    }

    @Test
    void activate_ko() {
        doThrow(new ActivateTokenNotValidException()).when(securityService).activate(RANDOM_TOKEN);
        assertThrows(ActivateTokenNotValidException.class,
                () -> controller.activate(RANDOM_TOKEN)
        );

        verify(securityService, times(1)).activate(RANDOM_TOKEN);
    }

    @Test
    void logout() {
        ResponseEntity<Void> response = controller.logout();
        assertEquals(HttpStatus.OK, response.getStatusCode());
        verify(securityService, times(1)).logout();
    }

    @Test
    void reset() {
        String password= "new Password";
        PasswordResetJsonRequest resetJsonRequest = new PasswordResetJsonRequest();
        resetJsonRequest.password = password;
        ResponseEntity<Void> response = controller.resetPassword(resetJsonRequest);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        verify(securityService, times(1)).resetPassword(password);
    }

    private UserLoginJsonRequest generateLoginRequest() {
        UserLoginJsonRequest request = new UserLoginJsonRequest();
        request.username = USER;
        request.password = PASSWORD;
        return request;
    }

    private RegisterSecurityUserJsonContract generateCreationRequest() {
        RegisterSecurityUserJsonContract request = new RegisterSecurityUserJsonContract();
        request.email = EMAIL;
        request.username = USER;
        request.password = PASSWORD;
        return request;
    }


//    ResponseEntity<Void> logout(HttpServletRequest request) {
//        request.getSession().removeAttribute(
//                SecurityFilter.getUserSessionAttributeKey(securityService.getCurrentUser())
//        );
//        return ResponseEntity.status(HttpStatus.OK).build();
//    }
}
