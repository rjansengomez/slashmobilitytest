package com.slashmobility.security.infrastructure.repository.validator;

import com.slashmobility.sdk.security.SecurityUser;
import com.slashmobility.security.domain.entity.UserLogin;
import com.slashmobility.security.infrastructure.repository.exception.ActivateTokenNotValidException;
import com.slashmobility.security.infrastructure.repository.exception.EmailAlreadyExistsException;
import com.slashmobility.security.infrastructure.repository.exception.LoginFailedException;
import com.slashmobility.security.infrastructure.repository.exception.UserAlreadyExistsException;
import com.slashmobility.security.infrastructure.repository.jpa.SecurityRepositoryJpa;
import com.slashmobility.security.infrastructure.repository.model.UserModel;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static com.slashmobility.security.utils.TestUtils.*;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
public class SecurityRepositoryValidatorTest {

    @Mock
    private SecurityRepositoryJpa repositoryJpa;

    @InjectMocks
    private SecurityRepositoryValidator validator;


    @Test
    void validatePassword_ok() {
        validator.validatePassword(PASSWORD, PASSWORD_ENCRYPTED);
    }

    @Test
    void validatePassword_ko() {
        assertThrows(LoginFailedException.class,
                () -> validator.validatePassword("OTHER", PASSWORD_ENCRYPTED)
        );
    }

    @Test
    void validateUserNotExists_ok() {
        UserLogin user = generateDefaultCreation();
        when(repositoryJpa.findById(user.getId())).thenReturn(empty());
        validator.validateUserNotExist(user);
    }

    @Test
    void validateUserNotExists_ko() {
        UserLogin user = generateDefaultCreation();
        when(repositoryJpa.findById(user.getId())).thenReturn(of(new UserModel()));
        assertThrows(UserAlreadyExistsException.class,
                () -> validator.validateUserNotExist(user)
        );
    }

    @Test
    void validateEmailIsUnique_ok() {
        when(repositoryJpa.findByEmail(EMAIL)).thenReturn(empty());
        validator.validateEmailIsUnique(EMAIL);
    }

    @Test
    void validateEmailIsUnique_ko() {
        when(repositoryJpa.findByEmail(EMAIL)).thenReturn(of(new UserModel()));
        assertThrows(EmailAlreadyExistsException.class,
                () -> validator.validateEmailIsUnique(EMAIL)
        );
    }

    @Test
    void validateActivationToken_ok() {
        when(repositoryJpa.findByActivationToken(RANDOM_TOKEN)).thenReturn(of(new UserModel()));
        validator.validateActivationToken(RANDOM_TOKEN);
    }

    @Test
    void validateActivationToken_ko() {
        when(repositoryJpa.findByActivationToken(RANDOM_TOKEN)).thenReturn(empty());
        assertThrows(ActivateTokenNotValidException.class,
                () -> validator.validateActivationToken(RANDOM_TOKEN)
        );
    }

    private UserLogin generateDefaultCreation() {
        return UserLogin.builder()
                .id(USER)
                .build();
    }
}
