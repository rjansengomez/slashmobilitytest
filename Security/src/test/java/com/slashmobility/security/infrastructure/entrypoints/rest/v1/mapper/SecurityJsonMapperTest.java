package com.slashmobility.security.infrastructure.entrypoints.rest.v1.mapper;

import com.slashmobility.contracts.json.v1.RegisterSecurityUserJsonContract;
import com.slashmobility.security.domain.entity.UserLogin;
import com.slashmobility.security.infrastructure.entrypoints.rest.v1.json.UserLoginJsonRequest;
import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static com.slashmobility.security.utils.TestUtils.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class SecurityJsonMapperTest {
    private SecurityJsonMapper mapper = new SecurityJsonMapper();

    @Test
    void mapLoginJsonToDomain() {
        UserLoginJsonRequest request = generateDefaultRequest();
        UserLogin result = mapper.mapJsonToDomain(request);
        assertEquals(request.username, result.getSecurityUser().getUsername());
        new BCryptPasswordEncoder().matches(request.password, result.getPassword());
    }

    @Test
    void mapCreateJsonToDomain() {
        RegisterSecurityUserJsonContract request = generateDefaultRegisterRequest();
        UserLogin result = mapper.mapJsonToDomain(request);
        assertEquals(request.username, result.getSecurityUser().getUsername());
        assertEquals(request.email, result.getEmail());
        new BCryptPasswordEncoder().matches(request.password, result.getPassword());
    }

    private RegisterSecurityUserJsonContract generateDefaultRegisterRequest() {
        RegisterSecurityUserJsonContract request = new RegisterSecurityUserJsonContract();
        request.username = USER;
        request.email = EMAIL;
        request.password = PASSWORD;
        return request;
    }

    private UserLoginJsonRequest generateDefaultRequest() {
        UserLoginJsonRequest request = new UserLoginJsonRequest();
        request.password = PASSWORD;
        request.username = USER;
        return request;
    }
}

