package com.slashmobility.security.infrastructure.repository.mapper;

import com.slashmobility.sdk.security.SecurityUser;
import com.slashmobility.security.domain.entity.UserLogin;
import com.slashmobility.security.infrastructure.repository.model.UserModel;
import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static com.slashmobility.security.utils.TestUtils.*;
import static org.junit.jupiter.api.Assertions.*;

public class RepositoryMapperTest {

    private final RepositoryMapper mapper = new RepositoryMapper();

    @Test
    void mapDomainToRepo_ok() {
        UserLogin login = generateDefault();
        UserModel modelParsed = mapper.mapDomainToRepo(login);
        assertEquals(USER, modelParsed.getUsername());
        assertEquals(EMAIL, modelParsed.getEmail());
        assertFalse(modelParsed.isActive());
        assertFalse(modelParsed.isLogged());
        assertEquals(PASSWORD_ENCRYPTED, modelParsed.getPassword());
        assertNull(modelParsed.getActivationToken());
    }

    @Test
    void mapRepoToDomain_ok() {
        UserModel loginModel = generateUserModel();
        UserLogin userLogin = mapper.mapRepoToDomain(loginModel);
        assertEquals(USER, userLogin.getSecurityUser().getUsername());
        assertEquals(USER, userLogin.getId());
        assertEquals(EMAIL, userLogin.getEmail());
        assertTrue(userLogin.getActive());
        assertFalse(userLogin.getLogged());
        assertEquals(PASSWORD_ENCRYPTED, userLogin.getPassword());
    }

    private UserLogin generateDefault() {
        return UserLogin.builder()
                .id(USER)
                .email(EMAIL)
                .active(false)
                .logged(false)
                .securityUser(new SecurityUser(USER))
                .password(PASSWORD_ENCRYPTED)
                .build();
    }

    private UserModel generateUserModel() {
        UserModel model = new UserModel();
        model.setEmail(EMAIL);
        model.setLogged(false);
        model.setActive(true);
        model.setPassword(PASSWORD_ENCRYPTED);
        model.setUsername(USER);
        model.setActivationToken(RANDOM_TOKEN);
        return model;
    }
}
