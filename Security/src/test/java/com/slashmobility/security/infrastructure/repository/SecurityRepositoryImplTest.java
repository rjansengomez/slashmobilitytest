package com.slashmobility.security.infrastructure.repository;

import com.slashmobility.sdk.security.SecurityUser;
import com.slashmobility.security.domain.entity.UserLogin;
import com.slashmobility.security.infrastructure.repository.exception.ActivateTokenNotValidException;
import com.slashmobility.security.infrastructure.repository.exception.EmailAlreadyExistsException;
import com.slashmobility.security.infrastructure.repository.exception.LoginFailedException;
import com.slashmobility.security.infrastructure.repository.exception.UserAlreadyExistsException;
import com.slashmobility.security.infrastructure.repository.jpa.SecurityRepositoryJpa;
import com.slashmobility.security.infrastructure.repository.mapper.RepositoryMapper;
import com.slashmobility.security.infrastructure.repository.model.UserModel;
import com.slashmobility.security.infrastructure.repository.validator.SecurityRepositoryValidator;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static com.slashmobility.security.utils.TestUtils.*;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
public class SecurityRepositoryImplTest {

    @Mock
    private SecurityRepositoryJpa repositoryJpa;
    @Mock
    private RepositoryMapper repositoryMapper;
    @Mock
    private SecurityRepositoryValidator validator;

    @InjectMocks
    private SecurityRepositoryImpl securityRepository;


    @AfterEach
    void reset() {
        Mockito.reset(repositoryJpa);
        Mockito.reset(repositoryMapper);
        Mockito.reset(validator);
    }

    @Test
    void login_ok() {
        //Prepare
        UserLogin defaultLogin = generateDefaultLogin();
        UserModel userModelOk = generateUserOk();
        when(repositoryJpa.findById(defaultLogin.getId())).thenReturn(of(userModelOk));
        UserModel userModelLogged = generateUserOk();
        userModelLogged.setLogged(true);
        when(repositoryJpa.save(userModelLogged)).thenReturn(userModelLogged);
        when(repositoryMapper.mapRepoToDomain(userModelLogged)).thenReturn(map(userModelLogged));

        //Act
        UserLogin userLogged = securityRepository.login(defaultLogin);

        //Verifications
        verify(repositoryJpa, times(1)).findById(defaultLogin.getId());
        verify(repositoryJpa, times(1)).save(userModelLogged);
        verify(repositoryMapper, times(1)).mapRepoToDomain(userModelOk);
        verify(validator, times(1)).validatePassword(PASSWORD, PASSWORD_ENCRYPTED);

        //Assert
        assertEquals(USER, userLogged.getId());
        assertEquals(EMAIL, userLogged.getEmail());
        assertEquals(USER, userLogged.getSecurityUser().getUsername());
        assertTrue(userLogged.getActive());
        assertTrue(userLogged.getLogged());
    }

    @Test
    void login_failed_no_user() {
        //Prepare
        UserLogin defaultLogin = generateDefaultLogin();
        when(repositoryJpa.findById(defaultLogin.getId())).thenReturn(empty());

        //Act && Assert
        assertThrows(LoginFailedException.class,
                () -> securityRepository.login(defaultLogin)
        );

        //Verifications
        verify(repositoryJpa, times(1)).findById(defaultLogin.getId());
        verify(repositoryJpa, times(0)).save(any());
        verify(repositoryMapper, times(0)).mapRepoToDomain(any());
        verify(validator, times(0)).validatePassword(anyString(), anyString());
    }

    @Test
    void login_failed_user_not_active() {
        //Prepare
        UserLogin defaultLogin = generateDefaultLogin();
        UserModel userModelOk = generateUserOk();
        userModelOk.setActive(false);
        when(repositoryJpa.findById(defaultLogin.getId())).thenReturn(of(userModelOk));

        //Act && Assert
        assertThrows(LoginFailedException.class,
                () -> securityRepository.login(defaultLogin)
        );

        //Verifications
        verify(repositoryJpa, times(1)).findById(defaultLogin.getId());
        verify(repositoryJpa, times(0)).save(any());
        verify(repositoryMapper, times(0)).mapRepoToDomain(any());
        verify(validator, times(0)).validatePassword(anyString(), anyString());
    }

    @Test
    void login_failed_wrong_password() {
        //Prepare
        UserLogin defaultLogin = generateDefaultLogin();
        UserModel userModelOk = generateUserOk();
        doThrow(LoginFailedException.class).when(validator).validatePassword(PASSWORD, PASSWORD_ENCRYPTED);
        when(repositoryJpa.findById(defaultLogin.getId())).thenReturn(of(userModelOk));

        //Act && Assert
        assertThrows(LoginFailedException.class,
                () -> securityRepository.login(defaultLogin)
        );

        //Verifications
        verify(repositoryJpa, times(1)).findById(defaultLogin.getId());
        verify(repositoryJpa, times(0)).save(any());
        verify(repositoryMapper, times(0)).mapRepoToDomain(any());
        verify(validator, times(1)).validatePassword(anyString(), anyString());
    }


    @Test
    void create() {
        //Prepare
        UserLogin userToBeCreated = generateDefaultCreation();
        UserModel userToBeCreatedModel = generateUserOk();
        userToBeCreatedModel.setActive(false);
        userToBeCreatedModel.setActivationToken(null);
        when(repositoryMapper.mapDomainToRepo(userToBeCreated)).thenReturn(userToBeCreatedModel);

        //Act
        securityRepository.create(userToBeCreated, RANDOM_TOKEN);

        //Verifications
        userToBeCreatedModel.setActivationToken(RANDOM_TOKEN);
        verify(validator, times(1)).validateUserNotExist(userToBeCreated);
        verify(validator, times(1)).validateEmailIsUnique(userToBeCreated.getEmail());
        verify(repositoryMapper, times(1)).mapDomainToRepo(userToBeCreated);
        verify(repositoryJpa, times(1)).save(userToBeCreatedModel);
    }

    @Test
    void create_ko_user_exists() {
        //Prepare
        UserLogin userToBeCreated = generateDefaultCreation();
        doThrow(UserAlreadyExistsException.class).when(validator).validateUserNotExist(userToBeCreated);

        //Act
        assertThrows(UserAlreadyExistsException.class,
                () -> securityRepository.create(userToBeCreated, RANDOM_TOKEN)
        );

        //Verifications
        verify(validator, times(1)).validateUserNotExist(userToBeCreated);
        verify(validator, times(0)).validateEmailIsUnique(anyString());
        verify(repositoryMapper, times(0)).mapDomainToRepo(any());
        verify(repositoryJpa, times(0)).save(any());
    }

    @Test
    void create_ko_email_exists() {
        //Prepare
        UserLogin userToBeCreated = generateDefaultCreation();
        doThrow(EmailAlreadyExistsException.class).when(validator).validateEmailIsUnique(userToBeCreated.getEmail());

        //Act
        assertThrows(EmailAlreadyExistsException.class,
                () -> securityRepository.create(userToBeCreated, RANDOM_TOKEN)
        );

        //Verifications
        verify(validator, times(1)).validateUserNotExist(userToBeCreated);
        verify(validator, times(1)).validateEmailIsUnique(userToBeCreated.getEmail());
        verify(repositoryMapper, times(0)).mapDomainToRepo(any());
        verify(repositoryJpa, times(0)).save(any());
    }


    @Test
    void activate_ok() {
        //Prepare
        UserModel userModelOk = generateUserOk();
        userModelOk.setActive(false);
        when(validator.validateActivationToken(RANDOM_TOKEN)).thenReturn(userModelOk);
        UserModel userModelActivated = generateUserOk();
        when(repositoryJpa.save(userModelActivated)).thenReturn(userModelActivated);
        when(repositoryMapper.mapRepoToDomain(userModelActivated)).thenReturn(map(userModelActivated));

        //Act
        UserLogin user = securityRepository.activate(RANDOM_TOKEN);

        //Assert
        assertEquals(USER, user.getId());
        assertEquals(EMAIL, user.getEmail());
        assertEquals(USER, user.getSecurityUser().getUsername());
        assertTrue(user.getActive());
        assertFalse(user.getLogged());

        //Verifications
        verify(repositoryJpa, times(1)).save(userModelActivated);
        verify(repositoryMapper, times(1)).mapRepoToDomain(userModelActivated);
        verify(validator, times(1)).validateActivationToken(RANDOM_TOKEN);
    }

    @Test
    void activate_token_ko() {
        //Prepare
        UserModel userModelOk = generateUserOk();
        userModelOk.setActive(false);
        when(validator.validateActivationToken(RANDOM_TOKEN)).thenThrow(new ActivateTokenNotValidException());

        //Act && Assert
        assertThrows(ActivateTokenNotValidException.class,
                () -> securityRepository.activate(RANDOM_TOKEN)
        );

        //Verifications
        verify(repositoryJpa, times(0)).save(any());
        verify(repositoryMapper, times(0)).mapRepoToDomain(any());
        verify(validator, times(1)).validateActivationToken(RANDOM_TOKEN);
    }

    @Test
    void updateUser_updatePassword_ok() {
        UserLogin defaultUser = generateDefaultLogin();
        String newPassword = "newPassword";
        UserModel userModelOk = generateUserOk();
        defaultUser.updatePasswordFromRaw(newPassword);
        when(repositoryJpa.findByUsername(defaultUser.getSecurityUser().getUsername())).thenReturn(of(userModelOk));
        UserModel userModelUpdated = generateUserOk();
        userModelUpdated.setPassword(defaultUser.getPassword());
        when(repositoryJpa.save(userModelUpdated)).thenReturn(userModelUpdated);
        when(repositoryMapper.mapRepoToDomain(userModelUpdated)).thenReturn(map(userModelUpdated));

        //Act
        UserLogin userUpdated = securityRepository.update(defaultUser);

        //Verifications
        verify(repositoryJpa, times(1)).findByUsername(defaultUser.getSecurityUser().getUsername());
        verify(repositoryJpa, times(1)).save(userModelUpdated);
        verify(repositoryMapper, times(1)).mapRepoToDomain(userModelOk);

        //Assert
        assertEquals(USER, userUpdated.getId());
        assertEquals(EMAIL, userUpdated.getEmail());
        assertEquals(USER, userUpdated.getSecurityUser().getUsername());
        assertTrue(new BCryptPasswordEncoder().matches(newPassword, userUpdated.getPassword()));
        assertTrue(userUpdated.getActive());
        assertTrue(userUpdated.getLogged());
    }


    private UserLogin map(UserModel userModel) {
        return UserLogin.builder()
                .id(userModel.getUsername())
                .email(userModel.getEmail())
                .password(userModel.getPassword())
                .securityUser(new SecurityUser(userModel.getUsername()))
                .active(userModel.isActive())
                .logged(userModel.isLogged())
                .build();
    }
}
