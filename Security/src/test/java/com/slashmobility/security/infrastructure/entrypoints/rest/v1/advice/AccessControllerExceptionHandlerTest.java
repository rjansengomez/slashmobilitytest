package com.slashmobility.security.infrastructure.entrypoints.rest.v1.advice;

import com.slashmobility.sdk.exceptions.MessageWrapper;
import com.slashmobility.sdk.exceptions.SessionExpiredException;
import com.slashmobility.security.domain.exception.ActivationEmailSendException;
import com.slashmobility.security.infrastructure.repository.exception.ActivateTokenNotValidException;
import com.slashmobility.security.infrastructure.repository.exception.EmailAlreadyExistsException;
import com.slashmobility.security.infrastructure.repository.exception.LoginFailedException;
import com.slashmobility.security.infrastructure.repository.exception.UserAlreadyExistsException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AccessControllerExceptionHandlerTest {

    AccessControllerExceptionHandler handler = new AccessControllerExceptionHandler();

    @Test
    void handleLoginFailedException_ok() {
        MessageWrapper messageExpected = MessageWrapper.wrapInfo(new LoginFailedException());
        MessageWrapper message = handler.handleLoginFailedException(new LoginFailedException());
        assertEquals(messageExpected.getLevel(), message.getLevel());
        assertEquals(messageExpected.getMessage(), message.getMessage());
    }

    @Test
    void handleSessionExpiredException_ok() {
        MessageWrapper messageExpected = MessageWrapper.wrapInfo(new SessionExpiredException());
        MessageWrapper message = handler.handleSessionExpiredException(new SessionExpiredException());
        assertEquals(messageExpected.getLevel(), message.getLevel());
        assertEquals(messageExpected.getMessage(), message.getMessage());
    }

    @Test
    void handleBadRequestException_ok_email() {
        MessageWrapper messageExpected = MessageWrapper.wrapInfo(new EmailAlreadyExistsException());
        MessageWrapper message = handler.handleBadRequestException(new EmailAlreadyExistsException());
        assertEquals(messageExpected.getLevel(), message.getLevel());
        assertEquals(messageExpected.getMessage(), message.getMessage());
    }

    @Test
    void handleBadRequestException_ok_username() {
        MessageWrapper messageExpected = MessageWrapper.wrapInfo(new UserAlreadyExistsException());
        MessageWrapper message = handler.handleBadRequestException(new UserAlreadyExistsException());
        assertEquals(messageExpected.getLevel(), message.getLevel());
        assertEquals(messageExpected.getMessage(), message.getMessage());
    }

    @Test
    void handleBadRequestException_ok_active_token() {
        MessageWrapper messageExpected = MessageWrapper.wrapInfo(new ActivateTokenNotValidException());
        MessageWrapper message = handler.handleBadRequestException(new ActivateTokenNotValidException());
        assertEquals(messageExpected.getLevel(), message.getLevel());
        assertEquals(messageExpected.getMessage(), message.getMessage());

    }

    @Test
    void handleActivationEmailException_ok() {
        MessageWrapper messageExpected = MessageWrapper.wrapError(new ActivationEmailSendException(new Exception("Error")));
        MessageWrapper message = handler.handleActivationEmailException(new ActivationEmailSendException(new Exception("Error")));
        assertEquals(messageExpected.getLevel(), message.getLevel());
        assertEquals(messageExpected.getMessage(), message.getMessage());
    }


}
