package component.utils;

import com.slashmobility.sdk.exceptions.MessageWrapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class TestUtils {
    public static String createURLWithPort(int port, String uri) {
        return "http://localhost:" + port + uri;
    }

    public static void validateListOK(List<?> expectedList, ResponseEntity<?> responseToValidate) {
        assertEquals(HttpStatus.OK, responseToValidate.getStatusCode());
        List<?> bodyList = (List<?>) responseToValidate.getBody();
        assertNotNull(bodyList);
        assertFalse(bodyList.isEmpty());
        expectedList.forEach(expectedValueSingle -> {
            if (!bodyList.contains(expectedValueSingle)) {
                fail("Value expected not found :" + expectedValueSingle);
            }
        });
    }

    public static String addFilter(String url, String filter) {
        return url.concat("&").concat(filter);
    }

    public static void validateListOK(List<?> expectedList, List<?> responseToValidate) {
        assertFalse(responseToValidate.isEmpty());
        expectedList.forEach(expectedValueSingle -> {
            if (!responseToValidate.contains(expectedValueSingle)) {
                fail("Value expected not found :" + expectedValueSingle);
            }
        });
    }

    private static void validateError(MessageWrapper expected, MessageWrapper result) {
        assertEquals(expected.getLevel(), result.getLevel());
        assertEquals(expected.getMessage(), result.getMessage());
    }

    public static void validateError(
            MessageWrapper expected,
            ResponseEntity<MessageWrapper> response,
            HttpStatus expectedHttpCode) {
        assertEquals(expectedHttpCode, response.getStatusCode());
        assertNotNull(response.getBody());
        validateError(expected, response.getBody());
    }
}
