package component;

import com.slashmobility.contracts.json.v1.RegisterSecurityUserJsonContract;
import com.slashmobility.sdk.exceptions.MessageWrapper;
import com.slashmobility.sdk.security.SecurityCache;
import com.slashmobility.sdk.security.SecurityEncryptor;
import com.slashmobility.sdk.security.SecurityUser;
import com.slashmobility.sdk.security.repository.redis.RedisRepository;
import com.slashmobility.security.SecurityApplication;
import com.slashmobility.security.infrastructure.entrypoints.rest.v1.json.PasswordResetJsonRequest;
import com.slashmobility.security.infrastructure.entrypoints.rest.v1.json.UserLoginJsonRequest;
import com.slashmobility.security.infrastructure.repository.exception.UserAlreadyExistsException;
import com.slashmobility.security.infrastructure.repository.jpa.SecurityRepositoryJpa;
import com.slashmobility.security.infrastructure.repository.model.UserModel;
import component.container.RedisTestContainer;
import component.mock.EmailServiceMock;
import component.utils.TestUtils;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest(classes = {
        SecurityApplication.class,
//        RedisMockConfig.class,
        EmailServiceMock.class
}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("component")
@ExtendWith(SpringExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ContextConfiguration(initializers = RedisTestContainer.RedisTestContainerInitializer.class)
public class AccessComponentTest {
    private TestRestTemplate restTemplate = new TestRestTemplate();
    private HttpHeaders headers = new HttpHeaders();
    private final int port;


    @Autowired
    private EmailServiceMock emailServiceMock;
    @Autowired
    private SecurityRepositoryJpa securityRepositoryJpa;

    @Autowired
    private RedisRepository redisRepository;


    private final SecurityEncryptor encryptor;

    AccessComponentTest(@LocalServerPort int port,
                        @Value("${secret.jwt}") String secret,
                        @Value("${session.minutes}") int sessionMinutes) {
        this.port = port;
        encryptor = new SecurityEncryptor(secret, sessionMinutes);
    }

    @BeforeAll
    void setup() {
        headers.setContentType(MediaType.APPLICATION_JSON);
    }

    @AfterEach
    void resetHeader() {
        headers = new HttpHeaders();
    }

    @Test
    @DisplayName("Register a user OK")
    void register_ok() {
        registerUserOk("User 1", "email@email.com", "password");
    }

    private String registerUserOk(String user, String email, String password) {
        RegisterSecurityUserJsonContract request = generateDefaultUserRegistration(user, email);
        request.password = password;
        HttpEntity<RegisterSecurityUserJsonContract> entity = new HttpEntity<>(request, headers);
        ResponseEntity<Void> response = restTemplate.exchange(
                TestUtils.createURLWithPort(port, "/api/V1/register"),
                HttpMethod.POST, entity, Void.class);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertNull(response.getBody());
        assertTrue(emailServiceMock.emailSent());
        return emailServiceMock.getActivationToken();
    }

    @Test
    @DisplayName("Register a user KO, the username already exists")
    void register_ko_username_already_exists() {
        String userName = "User 2";
        String email = "email2@email.com";
        RegisterSecurityUserJsonContract request = generateDefaultUserRegistration(userName, email);
        registerUserOk(userName, email, request.password);
        emailServiceMock.resetSent();
        HttpEntity<RegisterSecurityUserJsonContract> entity = new HttpEntity<>(request, headers);
        ResponseEntity<MessageWrapper> responseError = restTemplate.exchange(
                TestUtils.createURLWithPort(port, "/api/V1/register"),
                HttpMethod.POST, entity, MessageWrapper.class);
        assertEquals(HttpStatus.BAD_REQUEST, responseError.getStatusCode());
        assertNotNull(responseError.getBody());
        MessageWrapper expected = MessageWrapper.wrapInfo(new UserAlreadyExistsException());
        assertEquals(expected.getLevel(), responseError.getBody().getLevel());
        assertEquals(expected.getMessage(), responseError.getBody().getMessage());
        assertFalse(emailServiceMock.emailSent());
    }

    @Test
    @DisplayName("Activate a user successfully")
    void activate_ok() {
        String user = "UserActivation";
        String email = "UserActivation@email.com";
        String password = "password";
        String activationToken = registerUserOk(user, email, password);
        activateUser(activationToken, user);
    }

    private void activateUser(String activationToken, String user) {
        ResponseEntity<String> response = restTemplate.exchange(
                TestUtils.createURLWithPort(port, "/api/activate?token=" + activationToken),
                HttpMethod.GET, null, String.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNull(response.getBody());
        validateUserActivated(user);
    }

    private void validateUserActivated(String userId) {
        UserModel user = securityRepositoryJpa.findById(userId)
                .orElseThrow(RuntimeException::new);
        assertTrue(user.isActive());
    }

    @Test
    @DisplayName("Login a user successfully")
    void login_ok() {
        String user = "UserLogin";
        String email = "UserLogin@email.com";
        String password = "password";
        createUserAndActivateAndLoginOK(user, email, password);
    }

    private String createUserAndActivateAndLoginOK(String user, String email, String password) {
        UserLoginJsonRequest request = new UserLoginJsonRequest();
        request.username = user;
        request.password = password;
        String activationToken = registerUserOk(user, email, password);
        activateUser(activationToken, user);
        HttpEntity<UserLoginJsonRequest> entity = new HttpEntity<>(request, headers);
        ResponseEntity<String> response = restTemplate.exchange(
                TestUtils.createURLWithPort(port, "/api/login"),
                HttpMethod.POST, entity, String.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        validateToken(user, response.getBody());
        SecurityUser expected = new SecurityUser(user);
        SecurityUser cacheUser = redisRepository.findById(SecurityCache.getUserSessionAttributeKey(expected), expected);
        assertNotNull(cacheUser);
        assertEquals(expected.getUsername(), cacheUser.getUsername());
        return response.getBody();
    }

    private String doLoginOK(String user, String password) {
        UserLoginJsonRequest request = new UserLoginJsonRequest();
        request.username = user;
        request.password = password;
        HttpEntity<UserLoginJsonRequest> entity = new HttpEntity<>(request, headers);
        ResponseEntity<String> response = restTemplate.exchange(
                TestUtils.createURLWithPort(port, "/api/login"),
                HttpMethod.POST, entity, String.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        validateToken(user, response.getBody());
        SecurityUser expected = new SecurityUser(user);
        SecurityUser cacheUser = redisRepository.findById(SecurityCache.getUserSessionAttributeKey(expected), expected);
        assertNotNull(cacheUser);
        assertEquals(expected.getUsername(), cacheUser.getUsername());
        return response.getBody();
    }

    @Test
    @DisplayName("Logout ok")
    void logout_ok() {
        String user = "UserLogout";
        String email = "UserLogout@email.com";
        String password = "password";
        String jwtToken = createUserAndActivateAndLoginOK(user, email, password);
        headers.add("Authorization", jwtToken);
        HttpEntity<UserLoginJsonRequest> entity = new HttpEntity<>(null, headers);
        ResponseEntity<Void> response = restTemplate.exchange(
                TestUtils.createURLWithPort(port, "/api/userlogout"),
                HttpMethod.POST, entity, Void.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        SecurityUser cacheUser = redisRepository.findById(SecurityCache.getUserSessionAttributeKey(new SecurityUser(user)), new SecurityUser(user));
        assertNull(cacheUser);
    }

    @Test
    @DisplayName("Logout ko user not logged in")
    void logout_ko_no_logged() {
        String user = "UserLogoutKO";
        String email = "UserLogoutKO@email.com";
        String password = "password";
        String jwtToken = createUserAndActivateAndLoginOK(user, email, password);
        headers = new HttpHeaders();
        headers.add("Authorization", jwtToken);
        HttpEntity<UserLoginJsonRequest> entity = new HttpEntity<>(null, headers);
        ResponseEntity<Void> response = restTemplate.exchange(
                TestUtils.createURLWithPort(port, "/api/userlogout"),
                HttpMethod.POST, entity, Void.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        SecurityUser cacheUser = redisRepository.findById(SecurityCache.getUserSessionAttributeKey(new SecurityUser(user)), new SecurityUser(user));
        assertNull(cacheUser);
        ResponseEntity<Void> error = restTemplate.exchange(
                TestUtils.createURLWithPort(port, "/api/userlogout"),
                HttpMethod.POST, entity, Void.class);
        assertEquals(HttpStatus.FORBIDDEN, error.getStatusCode());
    }

    @Test
    @DisplayName("Reset the password of the user.")
    void reset_password() {
        String user = "UserResetPassword";
        String email = user + "@email.com";
        String password = "password";
        String jwtToken = createUserAndActivateAndLoginOK(user, email, password);
        headers = new HttpHeaders();
        headers.add("Authorization", jwtToken);
        PasswordResetJsonRequest resetJsonRequest = new PasswordResetJsonRequest();
        resetJsonRequest.password = "newPassword";
        HttpEntity<PasswordResetJsonRequest> entity = new HttpEntity<>(resetJsonRequest, headers);
        ResponseEntity<Void> response = restTemplate.exchange(
                TestUtils.createURLWithPort(port, "/api/reset"),
                HttpMethod.POST, entity, Void.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        doLoginOK(user, resetJsonRequest.password);
    }


    private void validateToken(String user, String token) {
        SecurityUser securityUser = encryptor.getUserFromJWT(token);
        assertEquals(user, securityUser.getUsername());
    }


    private RegisterSecurityUserJsonContract generateDefaultUserRegistration(
            String userName,
            String email) {
        RegisterSecurityUserJsonContract request = new RegisterSecurityUserJsonContract();
        request.username = userName;
        request.email = email;
        request.password = "password";
        return request;
    }
}
