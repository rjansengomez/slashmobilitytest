package component.mock;

import com.slashmobility.security.domain.entity.UserLogin;
import com.slashmobility.security.domain.service.EmailService;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

@Primary
@Service
public class EmailServiceMock implements EmailService {

    private boolean emailSent = false;
    private String activationToken;

    @Override
    public void sendActivationEmail(UserLogin userLogin, String activationToken) {
        emailSent = true;
        this.activationToken = activationToken;
    }

    public boolean emailSent() {
        return emailSent;
    }

    public String getActivationToken() {
        return this.activationToken;
    }

    public void resetSent() {
        emailSent = false;
        this.activationToken = null;
    }
}
