package com.slashmobility.security.config;

import com.sendgrid.SendGrid;
import com.slashmobility.sdk.security.SecurityEncryptor;
import com.slashmobility.sdk.security.SecurityWrapper;
import lombok.Generated;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

@Configuration
@Generated
@Order(value = 1)
public class Config {

    @Bean
    public SendGrid getSendgridBean(@Value("${email.sengrid.apikey}") String apiKey) {
        return new SendGrid(apiKey);
    }

    @Bean
    SecurityWrapper securityWrapperBean() {
        return new SecurityWrapper();
    }

    @Bean
    SecurityEncryptor securityEncryptorBean(@Value(value = "${secret.jwt}") String secret,
                                            @Value(value = "${session.minutes}") int minutes) {
        return new SecurityEncryptor(secret, minutes);
    }
}
