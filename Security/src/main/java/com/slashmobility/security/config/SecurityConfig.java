package com.slashmobility.security.config;

import com.slashmobility.sdk.security.SecurityEncryptor;
import com.slashmobility.sdk.security.SecurityFilter;
import com.slashmobility.sdk.security.WebSecurityConfig;
import com.slashmobility.sdk.security.repository.redis.RedisRepository;
import lombok.Generated;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@Profile("!test")
@Configuration
@EnableWebSecurity
@Generated
public class SecurityConfig extends WebSecurityConfig {

    public SecurityConfig(final SecurityEncryptor encryptor,
                          final RedisRepository redisRepository) {
        super(new SecurityFilter(encryptor, redisRepository));
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/login", "/V1/register", "/activate",
                "/v2/api-docs",
                "/configuration/ui",
                "/swagger-resources/**",
                "/configuration/security",
                "/swagger-ui.html",
                "/webjars/**");
    }
}
