package com.slashmobility.security.domain.service;

import com.slashmobility.sdk.security.SecurityUser;
import com.slashmobility.security.domain.entity.UserLogin;

public interface SecurityService {

    String login(UserLogin userLogin);

    SecurityUser getCurrentUser();

    void create(UserLogin mapJsonToDomain);

    void activate(String activateToken);

    void logout();

    void resetPassword(String password);
}
