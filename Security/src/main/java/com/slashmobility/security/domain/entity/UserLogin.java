package com.slashmobility.security.domain.entity;

import com.slashmobility.sdk.security.SecurityEncryptor;
import com.slashmobility.sdk.security.SecurityUser;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@EqualsAndHashCode
@ToString
public class UserLogin {
    private String id;
    private SecurityUser securityUser;
    private String password;
    private String email;
    private Boolean active;
    private Boolean logged;

    private UserLogin(UserLoginBuilder builder) {
        this.id = builder.id;
        this.securityUser = builder.securityUser;
        this.password = builder.password;
        this.email = builder.email;
        this.active = builder.active;
        this.logged = builder.logged;
    }
    public static UserLoginBuilder builder() {
        return new UserLoginBuilder();
    }

    public static class UserLoginBuilder {
        private String id;
        private SecurityUser securityUser;
        private String password;
        private String email;
        private Boolean active;
        private Boolean logged;

        public UserLoginBuilder id(String id) {
            this.id = id;
            return this;
        }

        public UserLoginBuilder securityUser(SecurityUser securityUser) {
            this.securityUser = securityUser;
            return this;
        }

        public UserLoginBuilder password(String password) {
            this.password = password;
            return this;
        }

        public UserLoginBuilder passwordEncrypt(String password) {
            this.password = encryptPassword(password);
            return this;
        }

        public UserLoginBuilder email(String email) {
            this.email = email;
            return this;
        }

        public UserLoginBuilder active(Boolean active) {
            this.active = active;
            return this;
        }

        public UserLoginBuilder logged(Boolean logged) {
            this.logged = logged;
            return this;
        }

        public UserLogin build() {
            return new UserLogin(this);
        }
    }

    public void updatePasswordFromRaw(String password) {
        this.password = encryptPassword(password);
    }

    private static String encryptPassword(String password) {
        return SecurityEncryptor.encodePassword(password);
    }
}
