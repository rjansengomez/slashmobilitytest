package com.slashmobility.security.domain.service.impl;

import com.slashmobility.sdk.security.*;
import com.slashmobility.security.domain.entity.UserLogin;
import com.slashmobility.security.domain.repository.SecurityRepository;
import com.slashmobility.security.domain.service.EmailService;
import com.slashmobility.security.domain.service.SecurityService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SecurityServiceImpl implements SecurityService {

    private final SecurityWrapper wrapper;
    private final SecurityRepository repository;
    private final EmailService emailService;
    private final SecurityEncryptor encryptor;
    private final SecurityCache securityCache;

    public SecurityServiceImpl(final SecurityWrapper wrapper,
                               final SecurityRepository repository,
                               final EmailService emailService,
                               final SecurityEncryptor encryptor,
                               final CacheRepository cacheRepository
    ) {
        this.wrapper = wrapper;
        this.repository = repository;
        this.emailService = emailService;
        this.encryptor = encryptor;
        this.securityCache = new SecurityCache(cacheRepository);
    }

    @Override
    public String login(UserLogin userLogin) {
        UserLogin userLogged = repository.login(userLogin);
        wrapper.setLogin(userLogged.getSecurityUser());
        String jwt = encryptor.createJWT(userLogged.getSecurityUser());
        securityCache.storeUser(userLogged.getSecurityUser());
        return jwt;
    }

    @Override
    public SecurityUser getCurrentUser() {
        return wrapper.getCurrentUser();
    }

    @Override
    @Transactional
    public void create(UserLogin userLogin) {
        String activationToken = SecurityEncryptor.generateNewToken();
        UserLogin userForCreation = setDefaultCreationData(userLogin);
        repository.create(userForCreation, activationToken);
        emailService.sendActivationEmail(userForCreation, activationToken);
    }

    private UserLogin setDefaultCreationData(UserLogin userLogin) {
        return UserLogin.builder()
                .id(userLogin.getId())
                .email(userLogin.getEmail())
                .passwordEncrypt(userLogin.getPassword())
                .securityUser(userLogin.getSecurityUser())
                .active(false)
                .logged(false)
                .build();
    }

    @Override
    public void activate(String activateToken) {
        repository.activate(activateToken);
    }

    @Override
    public void logout() {
        securityCache.cleanLoginData(wrapper.getCurrentUser());
    }

    @Override
    public void resetPassword(String password) {
        UserLogin user = repository.findByUsername(wrapper.getCurrentUser().getUsername());
        user.updatePasswordFromRaw(password);
        repository.update(user);
    }
}
