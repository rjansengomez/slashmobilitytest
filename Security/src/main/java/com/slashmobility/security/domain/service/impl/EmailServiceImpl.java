package com.slashmobility.security.domain.service.impl;

import com.sendgrid.*;
import com.slashmobility.security.domain.entity.UserLogin;
import com.slashmobility.security.domain.exception.ActivationEmailSendException;
import com.slashmobility.security.domain.service.EmailService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class EmailServiceImpl implements EmailService {
    private final String activationUrl;
    private final SendGrid sendGrid;
    private final String emailFrom;
    private final String activationSubject;

    public EmailServiceImpl(
            @Value("${email.activation.url}") String activationUrl,
            @Value("${email.activation.from}") String emailFrom,
            @Value("${email.activation.subject}") String activationSubject,
            final SendGrid sendGrid
    ) {
        this.activationUrl = activationUrl;
        this.sendGrid = sendGrid;
        this.emailFrom = emailFrom;
        this.activationSubject = activationSubject;
    }

    @Override
    public void sendActivationEmail(UserLogin userLogin, String activationToken) {
        Content content = buildContent(activationToken);
        try {
            Request request = buildRequest(
                    buildMail(userLogin, content)
            );
            Response response = sendGrid.api(request);
            if (HttpStatus.ACCEPTED.value() != response.getStatusCode()) {
                throw new ActivationEmailSendException(
                        new Exception(response.getBody())
                );
            }
        } catch (IOException ex) {
            throw new ActivationEmailSendException(ex);
        }

    }

    private Request buildRequest(Mail mail) throws IOException {
        Request request = new Request();
        request.setMethod(Method.POST);
        request.setEndpoint("mail/send");
        request.setBody(mail.build());
        return request;

    }

    private Content buildContent(String activationToken) {
        return new Content(
                "text/plain",
                "To activate your account go to the following link: "
                        + activationUrl + "?token=" + activationToken);
    }

    Mail buildMail(UserLogin userLogin, Content content) {
        Email from = new Email(emailFrom);
        Email to = new Email(userLogin.getEmail());
        return new Mail(from, activationSubject, to, content);
    }
}
