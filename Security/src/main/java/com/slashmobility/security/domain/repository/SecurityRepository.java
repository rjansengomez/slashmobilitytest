package com.slashmobility.security.domain.repository;

import com.slashmobility.security.domain.entity.UserLogin;

public interface SecurityRepository {

    UserLogin login(UserLogin userLogin);

    void create(UserLogin userLogin, String activateToken);

    UserLogin update(UserLogin userLogin);

    UserLogin activate(String activateToken);

    UserLogin findByUsername(String username);
}
