package com.slashmobility.security.domain.service;

import com.slashmobility.security.domain.entity.UserLogin;

public interface EmailService {
    void sendActivationEmail(UserLogin userLogin, String activationToken);
}
