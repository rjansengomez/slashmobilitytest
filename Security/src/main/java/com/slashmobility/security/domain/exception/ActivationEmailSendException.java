package com.slashmobility.security.domain.exception;

public class ActivationEmailSendException extends RuntimeException {
    public ActivationEmailSendException(Exception ex) {
        super(ex);
    }
}
