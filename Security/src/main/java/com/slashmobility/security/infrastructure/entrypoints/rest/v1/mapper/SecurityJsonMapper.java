package com.slashmobility.security.infrastructure.entrypoints.rest.v1.mapper;

import com.slashmobility.contracts.json.v1.RegisterSecurityUserJsonContract;
import com.slashmobility.sdk.security.SecurityUser;
import com.slashmobility.security.domain.entity.UserLogin;
import com.slashmobility.security.infrastructure.entrypoints.rest.v1.json.UserLoginJsonRequest;
import org.springframework.stereotype.Component;


@Component
public class SecurityJsonMapper {
    public UserLogin mapJsonToDomain(UserLoginJsonRequest loginRequest) {
        return UserLogin.builder()
                .id(loginRequest.username)
                .securityUser(new SecurityUser(loginRequest.username))
                .password(loginRequest.password)
                .build();
    }

    public UserLogin mapJsonToDomain(RegisterSecurityUserJsonContract userRegisterRequest) {
        return UserLogin.builder()
                .id(userRegisterRequest.username)
                .email(userRegisterRequest.email)
                .securityUser(new SecurityUser(userRegisterRequest.username))
                .password(userRegisterRequest.password)
                .build();
    }
}
