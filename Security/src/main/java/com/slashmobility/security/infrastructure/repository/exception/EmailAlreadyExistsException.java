package com.slashmobility.security.infrastructure.repository.exception;

public class EmailAlreadyExistsException extends RuntimeException {

    private static final String MESSAGE = "The email already exists";

    public EmailAlreadyExistsException() {
        super(MESSAGE);
    }
}
