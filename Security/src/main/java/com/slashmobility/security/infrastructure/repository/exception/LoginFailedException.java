package com.slashmobility.security.infrastructure.repository.exception;

public class LoginFailedException extends RuntimeException {
    private final static String MESSAGE = "Username or password is incorrect";

    public LoginFailedException() {
        super(MESSAGE);
    }
}
