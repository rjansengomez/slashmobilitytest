package com.slashmobility.security.infrastructure.entrypoints.rest.v1.json;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class UserLoginJsonRequest {
    public String username;
    public String password;
}
