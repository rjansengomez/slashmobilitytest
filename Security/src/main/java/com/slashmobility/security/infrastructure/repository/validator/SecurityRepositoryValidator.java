package com.slashmobility.security.infrastructure.repository.validator;

import com.slashmobility.sdk.security.SecurityEncryptor;
import com.slashmobility.security.domain.entity.UserLogin;
import com.slashmobility.security.infrastructure.repository.exception.ActivateTokenNotValidException;
import com.slashmobility.security.infrastructure.repository.exception.EmailAlreadyExistsException;
import com.slashmobility.security.infrastructure.repository.exception.LoginFailedException;
import com.slashmobility.security.infrastructure.repository.exception.UserAlreadyExistsException;
import com.slashmobility.security.infrastructure.repository.jpa.SecurityRepositoryJpa;
import com.slashmobility.security.infrastructure.repository.model.UserModel;
import org.springframework.stereotype.Component;

@Component
public class SecurityRepositoryValidator {

    private final SecurityRepositoryJpa repositoryJpa;

    public SecurityRepositoryValidator(final SecurityRepositoryJpa repositoryJpa) {
        this.repositoryJpa = repositoryJpa;
    }

    public void validatePassword(String rawPassword, String encrypted) {
        if (!SecurityEncryptor.samePassword(rawPassword, encrypted)) {
            throw new LoginFailedException();
        }
    }

    public UserModel validateActivationToken(String activateToken) {
        return repositoryJpa.findByActivationToken(activateToken)
                .orElseThrow(ActivateTokenNotValidException::new);
    }

    public void validateUserNotExist(UserLogin userLogin) {
        repositoryJpa.findById(userLogin.getId()).ifPresent(x -> {
            throw new UserAlreadyExistsException();
        });
    }

    public void validateEmailIsUnique(String email) {
        repositoryJpa.findByEmail(email)
                .ifPresent(x -> {
                    throw new EmailAlreadyExistsException();
                });
    }
}
