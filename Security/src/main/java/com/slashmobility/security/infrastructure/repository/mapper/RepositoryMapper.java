package com.slashmobility.security.infrastructure.repository.mapper;

import com.slashmobility.sdk.security.SecurityUser;
import com.slashmobility.security.domain.entity.UserLogin;
import com.slashmobility.security.infrastructure.repository.model.UserModel;
import org.springframework.stereotype.Component;

@Component
public class RepositoryMapper {
    public UserModel mapDomainToRepo(UserLogin userLogin) {
        return new UserModel(userLogin);
    }

    public UserLogin mapRepoToDomain(UserModel userModel) {
        return UserLogin.builder()
                .id(userModel.getUsername())
                .password(userModel.getPassword())
                .email(userModel.getEmail())
                .securityUser(new SecurityUser(userModel.getUsername()))
                .active(userModel.isActive())
                .logged(userModel.isLogged())
                .build();
    }
}
