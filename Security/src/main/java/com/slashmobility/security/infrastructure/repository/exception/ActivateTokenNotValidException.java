package com.slashmobility.security.infrastructure.repository.exception;

public class ActivateTokenNotValidException extends RuntimeException {

    private static final String MESSAGE = "The activation token is incorrect";

    public ActivateTokenNotValidException() {
        super(MESSAGE);
    }
}
