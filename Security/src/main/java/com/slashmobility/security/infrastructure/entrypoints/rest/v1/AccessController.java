package com.slashmobility.security.infrastructure.entrypoints.rest.v1;

import com.slashmobility.contracts.json.v1.RegisterSecurityUserJsonContract;
import com.slashmobility.security.domain.service.SecurityService;
import com.slashmobility.security.infrastructure.entrypoints.rest.v1.json.PasswordResetJsonRequest;
import com.slashmobility.security.infrastructure.entrypoints.rest.v1.json.UserLoginJsonRequest;
import com.slashmobility.security.infrastructure.entrypoints.rest.v1.mapper.SecurityJsonMapper;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping
public class AccessController {

    private final SecurityService securityService;
    private final SecurityJsonMapper jsonMapper;

    public AccessController(final SecurityService securityService,
                            final SecurityJsonMapper jsonMapper) {
        this.securityService = securityService;
        this.jsonMapper = jsonMapper;
    }

    @PostMapping(value = "/login")
    @ApiOperation(value = "Login a user", response = Iterable.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully logged"),
            @ApiResponse(code = 400, message = "Information of the user is incorrect")
    })
    public ResponseEntity<String> login(
            @RequestBody UserLoginJsonRequest loginRequest
    ) {
        String token = securityService.login(jsonMapper.mapJsonToDomain(loginRequest));
        return ResponseEntity.ok(token);
    }

    @PostMapping(value = "/V1/register")
    @ApiOperation(value = "Register a user", response = Iterable.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully registered"),
            @ApiResponse(code = 400, message = "Information of the user is incorrect")
    })
    public ResponseEntity<Void> create(@RequestBody RegisterSecurityUserJsonContract registerRequest) {
        securityService.create(jsonMapper.mapJsonToDomain(registerRequest));
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    //THIS MUST BE A POST, but since in the email is sent an URL, this is to make easy the test.
    @GetMapping(value = "/activate")
    @ApiOperation(value = "Activate a user given the token", response = Iterable.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully activated"),
            @ApiResponse(code = 400, message = "Token is not valid")
    })
    public ResponseEntity<Void> activate(@RequestParam(value = "token") String activationToken) {
        securityService.activate(activationToken);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @PostMapping(value = "/userlogout")
    @ApiOperation(value = "Logout a user given the authorization", response = Iterable.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully logout"),
            @ApiResponse(code = 403, message = "Token or session are not valid")
    })
    @ApiImplicitParam(name = "Authorization", value = "Access Token",
            required = true, allowEmptyValue = false,
            paramType = "header", dataTypeClass = String.class,
            example = "Bearer access_token")
    //If wanted this can go to the DB
    public ResponseEntity<Void> logout() {
        securityService.logout();
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @PostMapping(value = "/reset")
    @ApiOperation(value = "Reset the password of a user given the authorization", response = Iterable.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully reset"),
            @ApiResponse(code = 403, message = "Token or session are not valid")
    })
    @ApiImplicitParam(name = "Authorization", value = "Access Token",
            required = true, allowEmptyValue = false,
            paramType = "header", dataTypeClass = String.class,
            example = "Bearer access_token")
    public ResponseEntity<Void> resetPassword(@RequestBody PasswordResetJsonRequest request) {
        securityService.resetPassword(request.password);
        return ResponseEntity.status(HttpStatus.OK).build();
    }
}
