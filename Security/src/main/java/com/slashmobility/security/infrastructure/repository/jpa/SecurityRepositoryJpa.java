package com.slashmobility.security.infrastructure.repository.jpa;

import com.slashmobility.security.infrastructure.repository.model.UserModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface SecurityRepositoryJpa extends JpaRepository<UserModel,String> {

    Optional<UserModel> findByActivationToken(String validationToken);

    Optional<UserModel> findByEmail(String email);

    Optional<UserModel> findByUsername(String username);
}
