package com.slashmobility.security.infrastructure.entrypoints.rest.v1.advice;

import com.slashmobility.sdk.exceptions.MessageWrapper;
import com.slashmobility.sdk.exceptions.SessionExpiredException;
import com.slashmobility.security.domain.exception.ActivationEmailSendException;
import com.slashmobility.security.infrastructure.entrypoints.rest.v1.AccessController;
import com.slashmobility.security.infrastructure.repository.exception.ActivateTokenNotValidException;
import com.slashmobility.security.infrastructure.repository.exception.EmailAlreadyExistsException;
import com.slashmobility.security.infrastructure.repository.exception.LoginFailedException;
import com.slashmobility.security.infrastructure.repository.exception.UserAlreadyExistsException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice(assignableTypes = AccessController.class)
@Slf4j
public class AccessControllerExceptionHandler {

    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(value = LoginFailedException.class)
    public MessageWrapper handleLoginFailedException(LoginFailedException ex) {
        log.info(ex.getMessage());
        return MessageWrapper.wrapInfo(ex);
    }

    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(value = SessionExpiredException.class)
    public MessageWrapper handleSessionExpiredException(SessionExpiredException ex) {
        log.info(ex.getMessage());
        return MessageWrapper.wrapInfo(ex);
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(value = {
            ActivationEmailSendException.class
    })
    public MessageWrapper handleActivationEmailException(ActivationEmailSendException ex) {
        log.error(ex.getMessage());
        return MessageWrapper.wrapError(ex);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = {
            UserAlreadyExistsException.class,
            EmailAlreadyExistsException.class,
            ActivateTokenNotValidException.class
    })
    public MessageWrapper handleBadRequestException(RuntimeException ex) {
        log.info(ex.getMessage());
        return MessageWrapper.wrapInfo(ex);
    }


    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(value = {
            RuntimeException.class
    })
    public MessageWrapper handleGeneralException(RuntimeException ex) {
        log.error(ex.getMessage());
        return MessageWrapper.wrapError(ex);
    }
}
