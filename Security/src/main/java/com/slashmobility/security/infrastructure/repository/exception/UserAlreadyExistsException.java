package com.slashmobility.security.infrastructure.repository.exception;

public class UserAlreadyExistsException extends RuntimeException {

    private static final String MESSAGE = "The username already exists";

    public UserAlreadyExistsException() {
        super(MESSAGE);
    }
}
