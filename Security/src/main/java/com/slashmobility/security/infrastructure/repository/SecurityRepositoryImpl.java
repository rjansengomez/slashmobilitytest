package com.slashmobility.security.infrastructure.repository;

import com.slashmobility.security.domain.entity.UserLogin;
import com.slashmobility.security.domain.repository.SecurityRepository;
import com.slashmobility.security.infrastructure.repository.exception.LoginFailedException;
import com.slashmobility.security.infrastructure.repository.exception.UserNotFoundException;
import com.slashmobility.security.infrastructure.repository.jpa.SecurityRepositoryJpa;
import com.slashmobility.security.infrastructure.repository.mapper.RepositoryMapper;
import com.slashmobility.security.infrastructure.repository.model.UserModel;
import com.slashmobility.security.infrastructure.repository.validator.SecurityRepositoryValidator;
import org.springframework.stereotype.Repository;

@Repository
public class SecurityRepositoryImpl implements SecurityRepository {

    private final SecurityRepositoryJpa repositoryJpa;
    private final RepositoryMapper repositoryMapper;
    private final SecurityRepositoryValidator validator;

    public SecurityRepositoryImpl(final SecurityRepositoryJpa repositoryJpa,
                                  final RepositoryMapper repositoryMapper,
                                  final SecurityRepositoryValidator validator) {
        this.repositoryJpa = repositoryJpa;
        this.repositoryMapper = repositoryMapper;
        this.validator = validator;
    }

    @Override
    public UserLogin login(UserLogin userLogin) {
        return repositoryJpa.findById(userLogin.getId())
                .filter(UserModel::isActive)
                .map(userModel -> doLogin(userLogin.getPassword(), userModel))
                .orElseThrow(LoginFailedException::new);
    }

    private UserLogin doLogin(String rawPassword, UserModel userModel) {
        validator.validatePassword(rawPassword, userModel.getPassword());
        return repositoryMapper.mapRepoToDomain(
                repositoryJpa.save(setLoginData(userModel))
        );
    }

    private UserModel setLoginData(UserModel userModel) {
        userModel.setLogged(true);
        return userModel;
    }

    @Override
    public void create(UserLogin userLogin, String activateToken) {
        validator.validateUserNotExist(userLogin);
        validator.validateEmailIsUnique(userLogin.getEmail());
        UserModel userToBeCreated = repositoryMapper.mapDomainToRepo(userLogin);
        userToBeCreated.setActivationToken(activateToken);
        repositoryJpa.save(userToBeCreated);
    }

    @Override
    public UserLogin update(UserLogin userLogin) {
        return repositoryMapper.mapRepoToDomain(
                repositoryJpa.save(repositoryMapper.mapDomainToRepo(userLogin))
        );
    }

    @Override
    public UserLogin activate(String activateToken) {
        UserModel userToActivate = validator.validateActivationToken(activateToken);
        userToActivate.setActive(true);
        return repositoryMapper.mapRepoToDomain(repositoryJpa.save(userToActivate));
    }

    @Override
    public UserLogin findByUsername(String username) {
        return repositoryJpa.findByUsername(username)
                .map(repositoryMapper::mapRepoToDomain)
                .orElseThrow(UserNotFoundException::new);
    }
}
