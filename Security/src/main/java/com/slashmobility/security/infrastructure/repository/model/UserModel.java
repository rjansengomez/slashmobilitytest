package com.slashmobility.security.infrastructure.repository.model;

import com.slashmobility.security.domain.entity.UserLogin;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;

@Getter
@Setter
@EqualsAndHashCode
@Entity(name = "access_user")
public class UserModel {
    @Id
    private String username;
    private String password;
    private String email;
    private boolean active;
    private boolean logged;
    private String activationToken;

    public UserModel(UserLogin userLogin) {
        this.username = userLogin.getSecurityUser().getUsername();
        this.password = userLogin.getPassword();
        this.active = userLogin.getActive();
        this.logged = userLogin.getLogged();
        this.email = userLogin.getEmail();
    }

    public UserModel() {
    }
}
