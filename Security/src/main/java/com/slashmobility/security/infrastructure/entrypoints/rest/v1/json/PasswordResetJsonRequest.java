package com.slashmobility.security.infrastructure.entrypoints.rest.v1.json;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class PasswordResetJsonRequest {
    @NotEmpty(message = "Password required")
    @ApiModelProperty(example = "newPassword", value = "New password value for the user logged.")
    public String password;
}
