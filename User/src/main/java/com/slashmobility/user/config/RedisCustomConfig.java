package com.slashmobility.user.config;

import com.slashmobility.sdk.security.repository.redis.config.RedisConfig;
import lombok.Generated;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;

@Configuration
@Generated
@EnableRedisRepositories
@Import(RedisConfig.class)
public class RedisCustomConfig {

}
