package com.slashmobility.user.config;

import com.slashmobility.sdk.security.SecurityEncryptor;
import com.slashmobility.sdk.security.SecurityFilter;
import com.slashmobility.sdk.security.WebSecurityConfig;
import com.slashmobility.sdk.security.repository.redis.RedisRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Profile("!test")
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfig {

    public SecurityConfig(final SecurityEncryptor encryptor,
                          final RedisRepository redisRepository) {
        super(new SecurityFilter(encryptor, redisRepository));
    }

    @Override
    public void configure(WebSecurity web) {
        web.ignoring().antMatchers("/register",
                "/v2/api-docs",
                "/configuration/ui",
                "/swagger-resources/**",
                "/configuration/security",
                "/swagger-ui.html",
                "/webjars/**");
    }
}
