package com.slashmobility.user.infrastructure.repository.jpa;

import com.slashmobility.user.infrastructure.repository.model.CustomerModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepositoryJpa extends JpaRepository<CustomerModel, String> {
}
