package com.slashmobility.user.infrastructure.repository;

import com.slashmobility.user.domain.entity.Customer;

public interface SecurityRepository {
    void createUser(Customer customer, String password);
}
