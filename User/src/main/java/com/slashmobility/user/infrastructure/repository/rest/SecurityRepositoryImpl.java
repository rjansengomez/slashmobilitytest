package com.slashmobility.user.infrastructure.repository.rest;

import com.slashmobility.contracts.json.v1.RegisterSecurityUserJsonContract;
import com.slashmobility.user.domain.entity.Customer;
import com.slashmobility.user.infrastructure.repository.SecurityRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

@Repository
public class SecurityRepositoryImpl implements SecurityRepository {
    private final String securityRegisterUrl;

    public SecurityRepositoryImpl(@Value("${access.security.url}") String url) {
        this.securityRegisterUrl = url;
    }

    @Override
    public void createUser(Customer customer,String password) {
        RestTemplate call = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        RegisterSecurityUserJsonContract request = createSecurityUserRequest(customer,password);
        HttpEntity<RegisterSecurityUserJsonContract> requestEntity = new HttpEntity<>(request);
        ResponseEntity<Void> response = call.exchange(securityRegisterUrl, HttpMethod.POST, requestEntity, Void.class);
    }

    private RegisterSecurityUserJsonContract createSecurityUserRequest(Customer customer,String password) {
        RegisterSecurityUserJsonContract contract = new RegisterSecurityUserJsonContract();
        contract.email = customer.getEmail();
        contract.password = password;
        contract.username = customer.getUsername();
        return contract;
    }
}
