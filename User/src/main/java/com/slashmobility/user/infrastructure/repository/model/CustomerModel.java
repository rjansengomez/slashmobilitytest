package com.slashmobility.user.infrastructure.repository.model;

import com.slashmobility.user.domain.entity.Customer;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "customer")
public class CustomerModel {
    @Id
    public String id;
    public String username;
    public String email;
    public String name;
    public String lastName;
    public String address;

    public CustomerModel(Customer customer) {
        id = customer.getId();
    }
}
