package com.slashmobility.user.domain.entity;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class Customer {
    private String id;
    private String username;
    private String email;
    private String name;
    private String lastName;
    private String address;
}
