package component.container;

import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.support.TestPropertySourceUtils;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.wait.strategy.Wait;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

public class RedisTestContainer extends GenericContainer<RedisTestContainer> {
    private static final String IMAGE_VERSION = "redis:alpine";
    private static RedisTestContainer container;
    private static int port;
    private static String containerIp;

    private RedisTestContainer() {
        super(IMAGE_VERSION);
        this.waitingFor(Wait.forListeningPort());
        this.withStartupTimeout(Duration.of(2, ChronoUnit.MINUTES));

    }

    public static RedisTestContainer getInstance() {
        if (container == null) {
            container = new RedisTestContainer();
        }
        return container;
    }

    public static class RedisTestContainerInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        @Override
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            getInstance();
            container.start();
            TestPropertySourceUtils.addInlinedPropertiesToEnvironment(
                    configurableApplicationContext,
                    "spring.redis.host" + containerIp,
                    "spring.redis.port=" + port);
        }
    }

    @Override
    public void start() {
        super.start();
        port = container.getMappedPort(6379);
        containerIp = container.getContainerIpAddress();
    }

    @Override
    public void stop() {
        //do nothing, JVM handles shut down
    }
}
