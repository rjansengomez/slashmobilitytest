package component;

import com.slashmobility.UserApplication;
import com.slashmobility.user.infrastructure.repository.jpa.CustomerRepositoryJpa;
import component.container.RedisTestContainer;
import com.slashmobility.sdk.security.test.LoginMock;
import component.mock.SecurityRepositoryMock;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;


@SpringBootTest(classes = {
        UserApplication.class,
        LoginMock.class,
        SecurityRepositoryMock.class
}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("component")
@ExtendWith(SpringExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ContextConfiguration(initializers = RedisTestContainer.RedisTestContainerInitializer.class)
public class UserComponentTest {
    private TestRestTemplate restTemplate = new TestRestTemplate();
    private HttpHeaders headers = new HttpHeaders();
    private final int port;

    @Autowired
    private CustomerRepositoryJpa securityRepositoryJpa;
    @Autowired
    private LoginMock loginMock;

    UserComponentTest(@LocalServerPort int port) {
        this.port = port;
    }

    @BeforeAll
    void setup() {
        headers.setContentType(MediaType.APPLICATION_JSON);
    }

    @AfterEach
    void resetHeader() {
        headers = new HttpHeaders();
    }

    @Test
    @DisplayName("Register a user OK")
    void register_ok() {
        String loginJwt = loginMock.mockLogin("registerUser");
    }

}
