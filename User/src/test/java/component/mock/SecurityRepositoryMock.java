package component.mock;

import com.slashmobility.user.domain.entity.Customer;
import com.slashmobility.user.infrastructure.repository.SecurityRepository;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

@Primary
@Repository
public class SecurityRepositoryMock implements SecurityRepository {

    public static boolean userCreated = false;

    @Override
    public void createUser(Customer customer) {
        userCreated = true;
    }
}
