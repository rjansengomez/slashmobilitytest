# SlashMobility Test

We are a big company which buys products to many different other companies. We need to store our providers and
products data. Additionally, as we have a big number of employees we request to Slashmobility to implement a
security system that verify who can access to our database. We have created a web application which will access to
the requested backend server.

## Authentication

First the user needs to be registered using username, email and password using the following endpoint,

`POST : http://localhost:8080/api/register`

```
Request:
{
   "username": "testuser",
   "password": "password",
   "email": "test@exmaple.com",
}
Response: 201
- Nothing
```

Second the user needs to be activated using the link provided with a token in the email using the following endpoint,

```
GET : http://localhost:8080/api/activate?token="Token provided"
Response: 200
 - Nothing
```

Third the user needs to be authenticated using username and password using the following endpoint,

`POST : http://localhost:8080/api/login`

```
Request:
{
   "username": "testuser",
   "password": "password"
}

Response: 200
{
    "token"
}
```
The user can reset its password once authenticated using the following endpoint,

`POST : http://localhost:8080/api/reset`

```
Request:
{
   "password": "password"
}
Response: 200
- Nothing
```
